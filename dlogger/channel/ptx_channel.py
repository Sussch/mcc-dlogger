# Data logger channel for PT100 / PT1000 temperature sensors.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from dlogger.channel.generic_channel import Channel
from dlogger.calibration.temperature.rtd import PTX


class PTXChannel(Channel):
    """Class for a PT100 / PT1000 measurement channel.
    Note that by default it outputs temperature in Kelvin."""

    def __init__(self, nickname, source, r0, a=None, b=None, c=None, max_residual=0.1, formula="x", unit="K"):
        """Initialize the measurement channel."""
        Channel.__init__(self, nickname, source, formula, unit, class_name="PTXChannel")
        self.global_scope["kelvin_to_celsius"] = lambda x: x - PTX.K_0C
        self.global_scope["celsius_to_kelvin"] = lambda x: x + PTX.K_0C

        self.max_residual = max_residual

        # Create a PTx calibration class with the coefficients.
        self.ptx = PTX(r0, a, b, c)

        self.log.info("Fitting PTx conversion polynomial for channel {}".format(nickname))
        if not self.ptx.fit(max_residual):
            msg = ".. Failed to find a polynomial of rank {}-{} with max residual {}. Rank {} has residuals {}."\
                .format(self.ptx.POLY_MIN_RANK, self.ptx.POLY_MAX_RANK,
                        max_residual, self.ptx.fit_rank, self.ptx.fit_residuals)
            self.log.error(msg)
        else:
            msg = ".. Found polynomial of rank {} with residuals {}.".format(self.ptx.fit_rank, self.ptx.fit_residuals)
            self.log.info(msg)

    def apply_calib(self, raw_value):
        """Apply calibration formula for the given raw value.

        Returns:
            Pair of corrected value and its unit.
        """
        # Only allow for the calibration of non-strings
        if not isinstance(raw_value, basestring):
            local_scope = {'x': self.ptx.temperature(raw_value)}
            return self.nickname, eval(self.formula, self.global_scope, local_scope), self.unit
        else:
            return self.nickname, raw_value, self.unit
