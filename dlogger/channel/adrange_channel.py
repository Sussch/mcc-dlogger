# Data logger channel with A/D range parameters.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from dlogger.channel.generic_channel import Channel


class ADRangeChannel(Channel):
    """Generic base class for measurement channel."""

    def __init__(self, nickname, source, ad_range, formula="x", unit=""):
        """Initialize a measurement channel."""
        Channel.__init__(self, nickname, source, formula, unit, class_name="ADRangeChannel")

        # Make sure that the range is given in the correct order.
        a, b, ad_range_id = ad_range
        if a > b:
            self.ad_range = (b, a, ad_range_id)
        else:
            self.ad_range = ad_range

    def get_ad_range(self):
        """Get A/D dynamic range (min, max, id)."""
        return self.ad_range

