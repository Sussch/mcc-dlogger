# CSV storage for DLogger.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import time
import os

from dlogger.storage.generic_storage import Storage


class CSV(Storage):
    """CSV storage for measurements."""

    # Minimum configurable period for flushing measurements to output file.
    MIN_FLUSH_PERIOD = 0.1

    def __init__(self, nickname):
        """Initialize the storage."""
        Storage.__init__(self, "CSV", nickname)

        self.delimiter = ';'

        self.file = None
        self.filepath = None
        self.flush_period = None
        self.last_write_time = None
        self.skip_header = False

    def open(self, filepath, flush_period=1):
        """Open a CSV file. Creates a CSV header when the file does not exist yet (no consistency checks afterwards).

           Args:
              filepath (str): Path to the output file.
              flush_period (:obj:`float`, optional): Period for flushing buffered data to file, in seconds.
                This helps to reduce disk I/O.
        """
        self.filepath = filepath

        self.set_flush_period(flush_period)

        # File already exists? Skip the header, then.
        self.skip_header = os.path.exists(filepath)

        try:
            if self.file is not None:
                self.close()

            self.file = open(self.filepath, "a+")
        except Exception as e:
            self.log.exception("Failed to open {} for CSV storage: {}".format(filepath, e))
            self.close()

    def set_flush_period(self, flush_period):
        """Set the time limit for flushing data into the output file."""
        if flush_period < self.MIN_FLUSH_PERIOD:
            self.log.warning(
                "Flush period {} is not supported. Using the minimum period of {}."
                .format(flush_period, self.MIN_FLUSH_PERIOD)
            )
            flush_period = self.MIN_FLUSH_PERIOD
        self.flush_period = flush_period

    def set_delimiter(self, delimiter=';'):
        """Set the CSV delimiter for compatibility."""
        self.delimiter = delimiter

    def write(self, row):
        """Store a row of measurements into the CSV."""
        if self.file is None:
            self.log.error(
                "Cannot write to CSV, please use open(\"{}\") on the CSV storage class first."
                .format(self.filepath)
            )
            return

        try:
            # Write header.
            if not self.skip_header:
                self.log.debug("File {} doesn't exist yet, creating header..".format(self.filepath))
                self.file.write(self.delimiter.join(self.column_headers) + '\n')
                self.skip_header = True

            # Write row.
            self.file.write(self.delimiter.join(str(x) for x in row) + '\n')

            # Flush to file, every now and then.
            if self.last_write_time is None:
                self.last_write_time = time.time()
            elif time.time() - self.last_write_time > self.flush_period:
                self.file.flush()
        except Exception as e:
            self.log.exception("Failed to write row to \"{}\": {}".format(self.filepath, e))

    def close(self):
        """Close the storage."""
        if self.file is not None:
            self.file.close()
            self.file = None
