# MCC USB-TEMP data logger device
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import random
import struct
import hid

from dlogger.device.generic_device import Device
from dlogger.channel.ptx_channel import PTXChannel


class MCC_USB_TEMP(Device):
    """MCC USB-TEMP DAQ temperature logger with a USB HID interface."""

    SUPPORTED_SOURCES = [
            ("CJC0", "DAQ internal temperature sensor 1 (*C)"), 
            ("CJC1", "DAQ internal temperature sensor 2 (*C)"), 
            ("R0", "Raw resistance value on CH0 (Ohm)"),
            ("R1", "Raw resistance value on CH1 (Ohm)"),
            ("R2", "Raw resistance value on CH2 (Ohm)"),
            ("R3", "Raw resistance value on CH3 (Ohm)"),
            ("R4", "Raw resistance value on CH4 (Ohm)"),
            ("R5", "Raw resistance value on CH5 (Ohm)"),
            ("R6", "Raw resistance value on CH6 (Ohm)"),
            ("R7", "Raw resistance value on CH7 (Ohm)"),
            ("T0", "DAQ calibrated temperature on CH0 (*C)"),
            ("T1", "DAQ calibrated temperature on CH1 (*C)"),
            ("T2", "DAQ calibrated temperature on CH2 (*C)"),
            ("T3", "DAQ calibrated temperature on CH3 (*C)"),
            ("T4", "DAQ calibrated temperature on CH4 (*C)"),
            ("T5", "DAQ calibrated temperature on CH5 (*C)"),
            ("T6", "DAQ calibrated temperature on CH6 (*C)"),
            ("T7", "DAQ calibrated temperature on CH7 (*C)"),
            ]

    def __init__(self, name="MCC_TEMP1", simulate=False):
        """Initialize an MCC USB-TEMP DAQ without connecting to it yet."""
        Device.__init__(self, "MCC_USB_TEMP", name, simulate)

        self.dev = None

    def add_ptx_channel(self, name, source, r0, a=None, b=None, c=None, max_residual=0.1, formula="x", unit=""):
        """Add a PT100 / PT1000 measurement channel with calibration coefficients."""
        # TODO:: Check against channel count
        c = PTXChannel(name, source, r0, a, b, c, max_residual, formula, unit)
        self.channels.append(c)

    def open(self):
        """Look for a USB device and connect to it."""
        if not self.simulate:
            self.log.debug("Looking for USB-TEMP (vendor 0x09DB, product 0x008D)..")

            # Find the device
            self.dev = hid.device()
            self.dev.open(0x09DB, 0x008D)

            self.log.info("Found {} {} ({})".format(
                self.dev.get_manufacturer_string(),
                self.dev.get_product_string(),
                self.dev.get_serial_number_string()))
        else:
            self.log.info("Simulating a DAQ..")
            random.seed()

    def close(self):
        """Disconnect from the USB device (if any)."""
        if not self.simulate:
            self.log.debug("Closing USB-TEMP..")
            if self.dev:
                self.dev.close()

    def blink_led(self):
        """Blinks the LED four times."""
        if not self.simulate:
            self.dev.write([0x40, 0x00])

    def get_resistance(self, channel):
        """Read resistance on a channel."""
        if not self.simulate:
            self.dev.write([0x18, channel, 0x01])
            data = self.dev.read(5)
            if data[0] != 0x18:
                self.log.error("Invalid response from the device. Expected 0x18, received {}.".format(hex(data[0])))
                return 0.0

            raw_data = bytearray(data[1:])
            value, = struct.unpack("<f", raw_data)
            return value
        else:
            return 20000 + 100 * random.random()

    def get_temperature(self, channel):
        """Read temperature on a channel."""
        if not self.simulate:
            self.dev.write([0x18, channel, 0x00])
            data = self.dev.read(5)
            if data[0] != 0x18:
                self.log.error("Invalid response from the device. Expected 0x18, received {}.".format(hex(data[0])))
                return 0.0

            raw_data = bytearray(data[1:])
            value, = struct.unpack("<f", raw_data)
            return value
        else:
            return 20 + random.random()

    def get_int_temp(self, channel):
        """Read DAQ internal temperature."""
        if not self.simulate:
            self.dev.write([0x18, 0x80 + channel, 0x01])
            data = self.dev.read(5)
            if data[0] != 0x18:
                self.log.error("Invalid response from the device. Expected 0x18, received {}.".format(hex(data[0])))
                return 0.0

            raw_data = bytearray(data[1:])
            value, = struct.unpack("<f", raw_data)
            return value
        else:
            return 20 + random.random()

    def measure(self, source_type, source_index):
        """Measure a channel by its type and index."""
        raw_value = 0
        if source_type == "R":
            raw_value = self.get_resistance(source_index)
        elif source_type == "T":
            raw_value = self.get_temperature(source_index)
        elif source_type == "CJC":
            raw_value = self.get_int_temp(source_index)
        else:
            self.log.error("Unsupported source channel type '{}'.".format(source_type))
        return raw_value
