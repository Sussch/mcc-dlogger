# MCC USB-1608 data logger device, Windows port
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import random
from mcculw import ul
from mcculw.ul import ULError
from mcculw.enums import InterfaceType, ULRange

from dlogger.device.adrange_device import ADRangeDevice


class MCC_1608(ADRangeDevice):
    """MCC USB-1608GX-2AO DAQ voltage logger with a USB DAQ interface."""

    SUPPORTED_SOURCES = [
            ("U0", "Raw voltage on CH0 (V)"),
            ("U1", "Raw voltage on CH1 (V)"),
            ("U2", "Raw voltage on CH2 (V)"),
            ("U3", "Raw voltage on CH3 (V)"),
            ("U4", "Raw voltage on CH4 (V)"),
            ("U5", "Raw voltage on CH5 (V)"),
            ("U6", "Raw voltage on CH6 (V)"),
            ("U7", "Raw voltage on CH7 (V)"),
            ("U8", "Raw voltage on CH8 (V)"),
            ("U9", "Raw voltage on CH9 (V)"),
            ("U10", "Raw voltage on CH10 (V)"),
            ("U11", "Raw voltage on CH11 (V)"),
            ("U12", "Raw voltage on CH12 (V)"),
            ("U13", "Raw voltage on CH13 (V)"),
            ("U14", "Raw voltage on CH14 (V)"),
            ("U15", "Raw voltage on CH15 (V)"),
            ]

    SUPPORTED_RANGES = [
            (-10, 10, ULRange.BIP10VOLTS), (-5, 5, ULRange.BIP5VOLTS), 
            (-2, 2, ULRange.BIP2VOLTS), (-1, 1, ULRange.BIP1VOLTS)
            ]

    def __init__(self, name="MCC_1608", simulate=False):
        """Initialize an MCC USB-1608 DAQ without connecting to it yet."""
        ADRangeDevice.__init__(self, "MCC_1608", name, simulate)

        self.log.info("Using the MCC Universal Language package.")

        self.dev = None

    def open(self, serial=None):
        """Look for a USB device and connect to it."""
        if not self.simulate:
            if serial is not None:
                self.log.debug("Looking for MCC USB-1608 devices with serial {}..".format(serial))
            else:
                self.log.debug("Looking for the first MCC USB-1608 device..")

            try:
                ul.ignore_instacal()

                devices = ul.get_daq_device_inventory(InterfaceType.ANY)
                if len(devices) > 0:
                    for d in devices:
                        self.log.info("Found " + d.product_name + " (" + d.unique_id + ")")

                        # We've found the device that we were looking for?
                        if (serial is not None and d.unique_id == serial) or d.product_name.startswith("USB-1608"):
                            self.dev = d
                            ul.create_daq_device(self.dev_id, self.dev)
                            break
                else:
                    self.log.error("No MCC devices found")
            except ULError as e:
                self.log.error("Failed to open USB-1608: {}: {}".format(e.errorcode, e.message))
            except Exception as e:
                self.log.exception(e)
        else:
            self.log.info("Simulating a DAQ..")
            random.seed()

    def close(self):
        """Disconnect from the USB device (if any)."""
        if not self.simulate:
            self.log.debug("Closing MCC USB-1608..")
            ul.release_daq_device(self.dev_id)

    def blink_led(self):
        """Blinks the LED four times."""
        try:
            if not self.simulate:
                ul.flash_led(self.dev_id)
        except ULError as e:
            self.log.error("Failed to blink LED: {}: {}".format(e.errorcode, e.message))

    def get_voltage(self, channel, ad_range):
        """Read voltage on a channel."""
        if not self.simulate:
            try:
                return ul.v_in(self.dev_id, channel, ad_range[2])
            except ULError as e:
                self.log.error(
                    "Failed to measure voltage on channel {} with range {}: {}: {}"
                    .format(channel, ad_range, e.errorcode, e.message)
                )
        else:
            return ad_range[0] + (ad_range[1] - ad_range[0]) * random.random()
        return None

    def measure(self, source_type, source_index, range_desc):
        """Measure a channel by its type and index."""
        raw_value = 0
        if source_type == "U":
            raw_value = self.get_voltage(source_index, range_desc)
        else:
            self.log.error("Unsupported source channel type '{}'.".format(source_type))
        return raw_value
