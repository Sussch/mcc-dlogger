# Configuration interpreter
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging

from dlogger.device.mcc.usb_temp import MCC_USB_TEMP
from dlogger.device.mcc.usb_1608 import MCC_1608
from dlogger.device.clock import Clock
from dlogger.storage.csv import CSV


class Config:
    # Minimum configurable delay between measurement sequences.
    MIN_MEASUREMENT_PERIOD = 0.01

    # Supported storage classes. Implement and add more.
    STORAGE_CLASSES = {
            "CSV": CSV
            }

    # Supported device classes. Implement and add more.
    DEVICE_CLASSES = {
            "CLOCK": Clock,
            "MCC_USB_TEMP": MCC_USB_TEMP,
            "MCC_1608": MCC_1608,
            }

    def __init__(self, filepath):
        """Initialize the configuration parser."""
        self.log = logging.getLogger("DLogger.Cfg")

        self.measurement_period = 1.0

        self.filepath = ""
        self.code = ""
        self.devices = []
        self.storages = []

        # Functions and modules to be used in the config file
        self.global_scope = {
                "set_measurement_period": self.set_measurement_period,
                "add_storage": self.add_storage,
                "add_device": self.add_device,
                "math": __import__("math"),
                }
        self.local_scope = {}

        # Load the configuration
        if filepath != "":
            self.load(filepath)
        else:
            self.log.error("Missing configuration filepath. Please specify the path in command-line arguments.")

    def list_channels(self):
        """List channels from all devices."""
        chlist = []
        for d in self.devices:
            chlist.extend(d.list_channels())
        return chlist

    def num_channels(self):
        """Count the number of channels over all devices."""
        num = 0
        for d in self.devices:
            num += d.num_channels()
        return num

    def list_device_classes(self):
        """List available device classes."""
        dclist = []
        for key, val in self.DEVICE_CLASSES.iteritems():
            dclist.append(key)
        return dclist

    def text_device_classes(self):
        """List available device classes as text."""
        return ', '.join(self.list_device_classes())

    def list_storage_classes(self):
        """List available storage classes."""
        sclist = []
        for key, val in self.STORAGE_CLASSES.iteritems():
            sclist.append(key)
        return sclist

    def text_storage_classes(self):
        """List available storage classes as text."""
        return ', '.join(self.list_storage_classes())

    def validate_config(self):
        """Check if the config seems valid."""
        is_all_ok = True

        # Check if we have at least one device.
        if len(self.devices) == 0:
            self.log.error(
                "Configuration lacks devices. "
                "Please use add_device(\"name\") to add at least one of the following: {}"
                .format(self.text_device_classes())
            )
            return False

        # Check if all devices have at least one channel.
        for d in self.devices:
            if d.num_channels() < 1:
                self.log.error(
                    "Device {0} of class {1} lacks channels. Please use the device member function "
                    "add_channel(\"name\", source) to add at least one channel. "
                    "'{1}' supports the following sources:\n{2}"
                    .format(d.nickname, d.class_name, d.text_supported_sources())
                )
                is_all_ok = False

        # Check if we have at least one storage endpoint.
        if len(self.storages) == 0:
            self.log.error(
                "Configuration lacks storage endpoints. "
                "Please use add_storage(\"name\") to add at least one of the following: {}"
                .format(self.text_storage_classes())
            )
            return False

        return is_all_ok

    def load(self, filepath):
        """Load a configuration file."""
        self.filepath = filepath
        self.log.info("Loading configuration from file \"{}\"..".format(filepath))
        try:
            with open(filepath, "rt") as fi:
                self.code = fi.read()

                exec(self.code, self.global_scope, self.local_scope)

            # Is it valid?
            if not self.validate_config():
                return

            # By now we know all the channels.
            # Propagate them to storage endpoints.
            for s in self.storages:
                s.set_header(self.list_channels())
        except Exception as e:
            self.log.exception(e)

    def set_measurement_period(self, period):
        """Set measurement period, in seconds."""
        if period < self.MIN_MEASUREMENT_PERIOD:
            period = self.MIN_MEASUREMENT_PERIOD
        self.measurement_period = period

    def add_storage(self, class_name, nickname=""):
        """Add storage endpoint for measurements."""
        storage = None
        if class_name not in self.STORAGE_CLASSES:
            self.log.error(
                "Unknown storage class '{}'. Only the following storage classes are supported: {}"
                .format(class_name, self.text_storage_classes())
            )
        else:
            # If no nickname is supplied, then generate one automagically.
            if nickname == "":
                nickname = class_name + "_{}".format(len(self.storages))

            storage = self.STORAGE_CLASSES[class_name](nickname)

            self.storages.append(storage)
        return storage

    def add_device(self, class_name, nickname="", simulate=None):
        """Add a device for measurements."""
        device = None
        if class_name not in self.DEVICE_CLASSES:
            self.log.error(
                "Unknown device '{}'. Only the following device classes are supported: {}"
                .format(class_name, self.text_device_classes())
            )
        else:
            # If no nickname is supplied, then generate one automagically.
            if nickname == "":
                nickname = class_name + "_{}".format(len(self.devices))

            # The user may want to simulate the device.
            if simulate is None:
                device = self.DEVICE_CLASSES[class_name](nickname)
            else:
                device = self.DEVICE_CLASSES[class_name](nickname, simulate)

            self.devices.append(device)
        return device

    def get_devices(self):
        """Get list of devices."""
        return self.devices

    def get_storage_endpoints(self, class_name=""):
        """Get list of storage endpoints."""
        # Get all storage endpoints.
        if class_name == "":
            return self.storages
        # Get all endpoints of specific class.
        endpoints = []
        for s in self.storages:
            if s.class_name == class_name:
                endpoints.append(s)
        return endpoints
