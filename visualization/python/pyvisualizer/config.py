#!/usr/bin/python
#
# Python Visualizer configuration.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import json
import logging

from singleton import Singleton


class ConfigSeries:
    DEFAULT_STYLES = ["k-", "b-", "g-", "c-", "m-", "y-"]

    def __init__(self, index=0, cfg_dict=None):
        """Initialize the series config."""
        self.config = Config()

        if cfg_dict is not None:
            self.set_dict(cfg_dict)
        else:
            self.index = index
            self.name = "Series"
            self.visible = True

            self.x = 0
            self.x_name = ""
            self.y = 0
            self.y_name = ""
            self.y_axis_name = ""
            self.style = ""

            self.pick_style()

    def set_x_id(self, x_index):
        """Set series X index, updating its name automatically."""
        self.x = x_index
        self.x_name = self.config.get_column_by_id(self.x)

    def set_y_id(self, y_index):
        """Set series Y index, updating its name automatically."""
        self.y = y_index
        self.y_name = self.config.get_column_by_id(self.y)

        self.update_axis()
        self.update_name()

    def set_y_axis_name(self, y_axis_name):
        """Set Y axis name, updating its index automatically."""
        self.y_axis_name = y_axis_name

    def set_style(self, style):
        """Set style for the series."""
        if style in ["", None]:
            self.pick_style()
        else:
            self.style = style
    
    def pick_style(self):
        """Automatically pick a style for the series."""
        index = self.index % len(self.DEFAULT_STYLES)
        self.style = self.DEFAULT_STYLES[index]

    def update_name(self):
        """Update series name."""
        # TODO:: Something more sophisticated?
        self.name = self.y_name

    def update_axis(self):
        """Make sure that the Y-Axis corresponds to the Y selection."""
        unit = self.config.units[self.y]
        self.set_y_axis_name(unit)

    def get_dict(self):
        """Convert the active config into a dict."""
        d = {
                "index": self.index,
                "name": self.name,
                "style": self.style,
                "x": self.x,
                "y": self.y,
                "x_name": self.x_name,
                "y_name": self.y_name,
                "y_axis_name": self.y_axis_name,
                }
        return d

    def set_dict(self, d):
        """Set the active config from a dict."""
        self.index = d["index"]
        self.name = d["name"]
        self.style = d["style"]
        self.x = d["x"]
        self.y = d["y"]
        self.x_name = d["x_name"]
        self.y_name = d["y_name"]
        self.y_axis_name = d["y_axis_name"]


class Config(Singleton):
    def __init__(self):
        """Initialize the config singleton."""
        self.log = logging.getLogger("PV.Config")

        self.columns = []
        self.units = []
        self.unit_map = {}
        self.series = []

        self.datafile_path = ""

        self.autorefresh_enabled = False
        self.autorefresh_period = 1.0

    def set_columns(self, columns, units):
        """Set the list of columns and units."""
        self.columns = columns
        self.units = units

        self.unit_map = {}
        # Map units to column indexes
        for i in range(0, len(units)):
            u = units[i]
            if u not in self.unit_map:
                self.unit_map[u] = [i]
            else:
                self.unit_map[u].append(i)

        # Restore custom Y-Axis as well.
        for s in self.series:
            if s.y_axis_name not in self.unit_map:
                self.unit_map[s.y_axis_name] = [s.y]
            else:
                self.unit_map[s.y_axis_name].append(s.y)

    def list_x_axis(self):
        """List potential X-Axis columns."""
        return self.columns

    def list_y_axis(self):
        """List potential units from Y-Axis columns."""
        return list(self.unit_map.keys())
    
    def update_yaxis(self):
        """Make sure that the unit map is up to date with the latest Y-Axis changes."""
        for s in self.series:
            u = s.y_axis_name
            if u not in self.unit_map:
                self.unit_map[u] = [s.y]
            else:
                self.unit_map[u].append(s.y)

    def get_columns(self):
        """List column labels with units."""
        return self.columns

    def get_column_by_id(self, index):
        """Get column label by column index."""
        return self.columns[index]

    def get_id_by_column(self, name):
        """Get column index by label."""
        for i in range(0, len(self.columns)):
            if self.columns[i] == name:
                return i
        return -1

    def alloc_series(self):
        """Allocate for a new series."""
        # TODO:: More sophisticated logic to suggest good X and Y axis combinations.
        s = ConfigSeries(len(self.series))
        self.series.append(s)
        return s

    def remove_series(self, s):
        """Remove a series."""
        self.series.remove(s)

    def get_dict(self):
        """Convert active config to dictionary."""
        d = {
                "datafile_path": self.datafile_path,
                "autorefresh_enabled": self.autorefresh_enabled,
                "autorefresh_period": self.autorefresh_period,
                "series": []
                }

        for s in self.series:
            d["series"].append(s.get_dict())

        return d

    def set_dict(self, d):
        """Update active config from dictionary."""
        self.datafile_path = d["datafile_path"]
        self.autorefresh_enabled = d["autorefresh_enabled"]
        self.autorefresh_period = d["autorefresh_period"]

        for s in d["series"]:
            self.series.append(ConfigSeries(cfg_dict=s))

    def load(self, filepath):
        """Load config from a json config file."""
        self.log.info("Loading configuration from \"{}\".".format(filepath))

        with open(filepath, "rt") as fi:
            d = json.load(fi)

            self.set_dict(d)

    def save(self, filepath):
        """Save config to a json config file."""
        self.log.info("Saving configuration into \"{}\".".format(filepath))
        d = self.get_dict()

        with open(filepath, "wt") as fo:
            json.dump(d, fo)

