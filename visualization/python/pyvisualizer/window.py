#!/usr/bin/python
#
# Python Visualizer main window.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os
import logging
from PySide import QtGui, QtCore

from csv import CSV
from config import Config
from graph import GraphView
from cfg_panel import CfgPanel


class PVMainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)

        self.log = logging.getLogger("PV.Window")

        self.splitter = None
        self.statusbar = None
        self.graph_view = None
        self.cfg_panel = None
        self.menu = None
        self.refresh_timer = None

        try:
            self.config = Config()
            self.csv = CSV()

            self.init_ui()
            self.init_timer()
        except Exception as e:
            self.log.exception(e)

    def init_ui(self):
        """Initialize user interface."""

        self.setWindowTitle("MCC Data Logger Python Visualizer")

        # Add a horizontal splitter.
        self.splitter = QtGui.QSplitter(self)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)

        self.init_graph_view()
        self.init_cfg_panel()

        # Set the main widget as a central widget
        self.splitter.setFocus()
        self.setCentralWidget(self.splitter)

        self.statusbar = self.statusBar()
        self.init_menu()

    def init_graph_view(self):
        """Initialize the matplotlib canvas."""
        self.graph_view = GraphView(self)
        self.splitter.addWidget(self.graph_view)
        self.connect(self.graph_view, QtCore.SIGNAL("onPick(int, float)"), self.on_pick)

    def init_cfg_panel(self):
        """Initialize the configuration panel."""
        self.cfg_panel = CfgPanel(self)
        self.cfg_panel.sig_series_updated.connect(self.series_updated)
        self.cfg_panel.sig_series_removed.connect(self.series_removed)
        self.cfg_panel.sig_series_visibility.connect(self.series_visibility)
        self.cfg_panel.sig_autorefresh_updated.connect(self.autorefresh_updated)
        self.cfg_panel.sig_refresh.connect(self.do_refresh)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.cfg_panel)

    def init_menu(self):
        """Initialize menu."""
        self.menu = self.menuBar()

        open_action = QtGui.QAction("&Open file", self)
        open_action.setShortcut("Ctrl+O")
        open_action.setStatusTip("Open a configuration or data file")
        open_action.triggered.connect(self.load_file)

        save_action = QtGui.QAction("&Save file", self)
        save_action.setShortcut("Ctrl+S")
        save_action.setStatusTip("Save configuration to file")
        save_action.triggered.connect(self.save_file)

        quit_action = QtGui.QAction("&Quit", self)
        quit_action.setShortcut("Ctrl+Q")
        quit_action.setStatusTip("Quit to DOS")
        quit_action.triggered.connect(self.exit)

        menu_file = self.menu.addMenu("&File")
        menu_file.addAction(open_action)
        menu_file.addAction(save_action)
        menu_file.addSeparator()
        menu_file.addAction(quit_action)

    def init_timer(self):
        """Initialize auto-refresh timer."""
        self.refresh_timer = QtCore.QTimer()
        self.refresh_timer.setSingleShot(False)
        self.refresh_timer.timeout.connect(self.do_refresh)

    def keyPressEvent(self, event):
        """Called on each keypress."""
        super(QtGui.QMainWindow, self).keyPressEvent(event)

    def on_pick(self):
        pass

    def load_file(self):
        """Load a CSV or configuration file."""
        filepath, _ = QtGui.QFileDialog.getOpenFileName(
            self,
            "Select a config file or a measurement log",
            "../..",
            "Any supported file (*.pvcfg *.csv);;Comma Separated Values (*.csv);;Configuration file (*.pvcfg)"
        )
        if len(filepath) > 0:
            self.log.info("Loading \"{}\"".format(filepath))
            
            filename, extension = os.path.splitext(filepath)
            if extension == ".pvcfg":
                # Remove old series.
                self.cfg_panel.clear()

                # Load the config file.
                self.config.load(filepath)
                # Load the same CSV file again.
                self.csv.load(self.config.datafile_path)

                # Update the config, based on the CSV header.
                self.config.set_columns(self.csv.get_header(), self.csv.get_units())
                # Restore config panel contents, based on the configuration class.
                self.cfg_panel.restore()

                # Propagate the data to the figure.
                self.graph_view.sig_update_data.emit(self.csv.get_data())
                # Add series on the figure.
                for s in self.config.series:
                    self.graph_view.sig_update_series.emit(s)
            else:
                # Load the CSV.
                self.csv.load(filepath)

                # Remember the CSV path.
                self.config.datafile_path = filepath
                # Update the config, based on the CSV header.
                self.config.set_columns(self.csv.get_header(), self.csv.get_units())

                # Propagate the data to the figure.
                self.graph_view.sig_update_data.emit(self.csv.get_data())

    def save_file(self):
        """Save a configuration or image file."""
        filepath, _ = QtGui.QFileDialog.getSaveFileName(
            self,
            "Select a config file",
            "../..",
            "Configuration file (*.pvcfg)"
        )
        filename, extension = os.path.splitext(filepath)
        if len(filepath) > 0:
            # Make sure that the file extension is there.
            if extension != ".pvcfg":
                filepath += ".pvcfg"

            self.log.info("Saving \"{}\"".format(filepath))
            self.config.save(filepath)

    def series_updated(self, series):
        """Propagate series config updates to the graph view."""
        self.graph_view.sig_update_series.emit(series)

    def series_visibility(self, series, is_visible):
        """Propagate series visibility updates to the graph view."""
        self.graph_view.sig_showhide_series.emit(series, is_visible)

    def series_removed(self, series):
        """Propagate series removal signals to the graph view."""
        self.graph_view.sig_remove_series.emit(series)

    def autorefresh_updated(self, enabled, period):
        """Update the state of the auto-refresh timer."""
        if enabled:
            self.log.info("Auto-refresh every {} s".format(period))
            self.refresh_timer.start(period * 1000.0)
        else:
            self.log.info("Auto-refresh disabled.")
            self.refresh_timer.stop()

    def do_refresh(self):
        """Redraw the figure."""
        self.csv.load(self.config.datafile_path)
        self.graph_view.sig_update_data.emit(self.csv.get_data())

    def exit(self):
        """Exit the program."""
        self.log.info("Closing down..")
        self.close()
