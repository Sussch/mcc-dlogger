#!/usr/bin/python
#
# Python Visualizer graph view.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from PySide import QtGui, QtCore

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

from config import Config


class GraphSeries:
    SUBPLOTS = []
    SUBPLOT_MAP = {}

    def __init__(self, parent, series_cfg):
        """Initialize the series."""
        self.parent = parent
        self.x = []
        self.y = []

        self.figure = None
        self.subplot = None
        self.legend = None
        self.line = None
        self.unit = None

        self.series_cfg = series_cfg

    def add_subplot(self):
        """Add a new subplot."""
        # Dynamic updates of subplots based on:
        #  https://stackoverflow.com/questions/12319796/dynamically-add-create-subplots-in-matplotlib

        # Update the geometry of the previous subplots.
        self.update_subplot_geometries()

        # Create a new subplot.
        n = len(GraphSeries.SUBPLOTS)
        self.subplot = self.figure.add_subplot(n + 1, 1, n + 1)
        # Remember it.
        GraphSeries.SUBPLOTS.append(self.subplot)
        GraphSeries.SUBPLOT_MAP[self.series_cfg.y_axis_name] = self.subplot

    def update_subplot_geometries(self):
        """Ensure that all the subplots are configured according to the number of rows."""
        n = len(GraphSeries.SUBPLOTS)
        for i in range(0, n):
            self.figure.axes[i].change_geometry(n + 1, 1, i + 1)

    def init_plot(self, figure):
        """Initialize the plot."""
        self.figure = figure

        self.unit = self.series_cfg.y_axis_name
        if self.unit not in GraphSeries.SUBPLOT_MAP:
            self.add_subplot()
        else:
            self.subplot = GraphSeries.SUBPLOT_MAP[self.unit]

        self.subplot.grid(True)
        self.subplot.set_xlabel(self.series_cfg.x_name)
        self.subplot.set_ylabel(self.unit)
        # Plot an empty series.
        self.line, = self.subplot.plot([], [], self.series_cfg.style, label=self.series_cfg.name)
        # Make sure that the new plot shows up in the legend.
        self.update_legend()

    def remove_subplot(self):
        """Remove the whole subplot."""
        if self.subplot is not None:
            # Forget the subplot.
            GraphSeries.SUBPLOTS.remove(self.subplot)
            GraphSeries.SUBPLOT_MAP.pop(self.unit, None)
            # Remove the subplot from the figure.
            self.subplot.remove()
            del self.subplot
            self.subplot = None

            self.update_subplot_geometries()

    def remove(self, remove_subplot=True):
        """Remove the line."""
        # Remove the line (if any).
        if self.line is not None:
            self.line.remove()
            del self.line
            self.line = None
            # If the line was removed, make sure to update the legend as well.
            self.update_legend()

        # No lines left? Remove the empty subplot, then.
        if remove_subplot and len(self.subplot.get_lines()) == 0:
            self.remove_subplot()

    def set_visibility(self, visible):
        """Show / hide the plot."""
        self.line.set_visible(visible)
        self.subplot.relim()
        self.subplot.autoscale_view(True, True, True)
        self.update_legend()

    def reinit_plot(self):
        """Reinitialize the plot."""
        self.remove(False)
        self.init_plot(self.figure)

    def validate_data(self, data, x_index, y_index):
        """Skip invalid measurement points."""
        pairs = zip(data[x_index], data[y_index])
        pairs = [p for p in pairs if p[0] and p[1]]
        x, y = zip(*pairs)
        return x, y

    def update_data(self, data):
        """Update the plot with the latest data."""
        if self.series_cfg is not None:
            x, y = self.validate_data(data, self.series_cfg.x, self.series_cfg.y)

            if self.line is not None:
                self.line.set_xdata(x)
                self.line.set_ydata(y)

                self.subplot.relim()
                self.subplot.autoscale_view(True, True, True)

    def update_legend(self):
        """Redraw the legend."""
        if self.legend is not None:
            self.legend.remove()

        self.legend = self.subplot.legend()
        if self.legend is not None:
            self.legend.draggable(state=True)


class GraphView(QtGui.QWidget):
    sig_update_data = QtCore.Signal(object)
    sig_update_series = QtCore.Signal(object)
    sig_showhide_series = QtCore.Signal(object, bool)
    sig_remove_series = QtCore.Signal(object)

    def __init__(self, parent=None):
        """Initialize the graph view."""
        QtGui.QWidget.__init__(self, parent)

        self.config = Config()
        self.gseries = []

        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)

        self.figure = None
        self.figure_canvas = None
        self.navbar = None
        self.data = None
        self.init_figure()

        self.sig_update_data.connect(self.do_update_data)
        self.sig_update_series.connect(self.do_update_series)
        self.sig_showhide_series.connect(self.do_showhide_series)
        self.sig_remove_series.connect(self.do_remove_series)

    def init_figure(self):
        """Initialize the figure."""
        # Set font size smaller.
        matplotlib.rcParams['font.size'] = 10.0

        self.figure = Figure()
        self.figure_canvas = FigureCanvas(self.figure)

        # Set size policy
        self.figure_canvas.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.figure_canvas.updateGeometry()

        # Create the navigation bar on top.
        self.navbar = NavigationToolbar(self.figure_canvas, self)
        self.layout.addWidget(self.navbar)

        # And figure canvas in the middle.
        self.layout.addWidget(self.figure_canvas)

    def do_update_series(self, series):
        """Update the configuration of a series in the figure."""
        for gs in self.gseries:
            if gs.series_cfg == series:
                gs.reinit_plot()
                gs.update_data(self.data)
                self.do_refresh()
                return

        # Create a new series.
        gs = GraphSeries(self, series)
        gs.init_plot(self.figure)
        self.gseries.append(gs)
        # Update its data.
        gs.update_data(self.data)
        # Update navbar and redraw the figure.
        self.navbar.update()
        self.do_refresh()

    def do_showhide_series(self, series, visible):
        """Temporarily remove a series from the figure, or make it re-appear."""
        for gs in self.gseries:
            if gs.series_cfg == series:
                gs.set_visibility(visible)
                break
        self.do_refresh()

    def do_remove_series(self, series):
        """Remove a series from the figure."""
        for gs in self.gseries:
            if gs.series_cfg == series:
                gs.remove()
                self.gseries.remove(gs)
                break
        # Update navbar and redraw the figure.
        self.navbar.update()
        self.do_refresh()

    def do_update_data(self, data):
        """Update data points on the figure."""
        self.data = list(map(list, zip(*data)))
        for gs in self.gseries:
            gs.update_data(self.data)
        self.do_refresh()

    def do_refresh(self):
        """Redraw the figure."""
        self.figure_canvas.draw()
