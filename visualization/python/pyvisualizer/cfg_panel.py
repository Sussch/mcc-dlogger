#!/usr/bin/python
#
# Python Visualizer configuration panel.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from PySide import QtGui, QtCore

from config import Config
from autorefresh_button import AutoRefreshButton
from addseries_button import AddSeriesButton
from series_widget import SeriesWidget


class CfgPanel(QtGui.QDockWidget):
    # Manual refresh requested.
    sig_refresh = QtCore.Signal()
    # Series settings were updated.
    sig_series_updated = QtCore.Signal(object)
    # Series removed.
    sig_series_removed = QtCore.Signal(object)
    # Series visibility changed.
    sig_series_visibility = QtCore.Signal(object, bool)
    # Autorefresh config updated.
    sig_autorefresh_updated = QtCore.Signal(bool, int)

    def __init__(self, parent):
        """Initialize the configuration panel."""
        QtGui.QDockWidget.__init__(self)
        self.setParent(parent)

        self.config = Config()

        self.series = []

        self.panel = None
        self.series_panel = None
        self.hbox_layout = None
        self.hbox_series_layout = None
        self.button_refresh = None
        self.button_autorefresh = None
        self.button_add = None
        self.init_ui()

    def init_ui(self):
        """Initialize user interface."""
        self.setWindowTitle("Configuration")
        self.setFeatures(QtGui.QDockWidget.AllDockWidgetFeatures)
        self.setAllowedAreas(QtCore.Qt.BottomDockWidgetArea | QtCore.Qt.TopDockWidgetArea)

        # Set the widget of this panel
        self.panel = QtGui.QWidget(self)
        self.setWidget(self.panel)
        # Set its layout
        self.hbox_layout = QtGui.QHBoxLayout(self.panel)
        self.hbox_layout.setContentsMargins(2, 2, 2, 2)
        self.panel.setLayout(self.hbox_layout)

        # Refresh button
        self.button_refresh = QtGui.QToolButton()
        self.button_refresh.setIcon(QtGui.QIcon("pyvisualizer/icons/arrow-circle.png"))
        self.button_refresh.setAutoRaise(True)
        self.button_refresh.clicked.connect(self.request_refresh)
        self.hbox_layout.addWidget(self.button_refresh)

        # Auto-refresh button
        self.button_autorefresh = AutoRefreshButton(self)
        self.button_autorefresh.sig_autorefresh_set.connect(self.autorefresh_updated)
        self.hbox_layout.addWidget(self.button_autorefresh)

        # Layout for series widgets.
        self.series_panel = QtGui.QWidget(self)
        self.hbox_series_layout = QtGui.QHBoxLayout(self.series_panel)
        self.hbox_series_layout.setContentsMargins(2, 2, 2, 2)
        self.series_panel.setLayout(self.hbox_series_layout)
        self.hbox_layout.addWidget(self.series_panel)

        # Add button
        self.button_add = AddSeriesButton(self)
        self.hbox_layout.addWidget(self.button_add)
        self.button_add.clicked.connect(self.add_series)

        # Add spacing at the right end.
        self.hbox_layout.insertStretch(-1)

    def autorefresh_updated(self, enabled, period):
        """Propagate the autorefresh update signal."""
        self.sig_autorefresh_updated.emit(enabled, period)

    def request_refresh(self):
        """Request for a refresh."""
        self.sig_refresh.emit()

    def add_series(self):
        """Add a series widget to the configuraton panel."""
        # Add a series to the config.
        s = self.config.alloc_series()
        # Create the widget.
        sw = SeriesWidget(self, s)
        self.series.append(sw)
        # Add it to the layout.
        self.hbox_series_layout.addWidget(sw)
        # And connect its signals.
        self.connect_series_widget(sw)

    def connect_series_widget(self, sw):
        """Connect a series widget's signals to our listeners."""
        # Connect the update signal.
        sw.sig_series_update.connect(self.update_series)
        # Connect the removal signal.
        sw.sig_series_remove.connect(self.remove_series)
        # Connect the visibility change signal.
        sw.sig_series_visibility.connect(self.change_series_visibility)

    def remove_series(self, widget):
        """Remove a series widget from the configuration panel."""
        s = widget.series
        self.sig_series_removed.emit(s)

        self.hbox_series_layout.removeWidget(widget)
        self.series.remove(widget)
        del widget

    def update_series(self, series):
        """Propagate the series update signal."""
        self.sig_series_updated.emit(series)

    def change_series_visibility(self, series, is_visible):
        """Propagate the series visibility signal."""
        self.sig_series_visibility.emit(series, is_visible)

    def clear(self):
        """Remove all series."""
        while self.series:
            self.series[0].do_remove()

    def restore(self):
        """Restore the series from configuration."""
        for s in self.config.series:
            # Create the widget.
            sw = SeriesWidget(self, s)
            self.series.append(sw)
            # Add it to the layout.
            self.hbox_series_layout.addWidget(sw)
            # And connect its signals.
            self.connect_series_widget(sw)

