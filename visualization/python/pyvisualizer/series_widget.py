#!/usr/bin/python
#
# Python Visualizer series widget.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from PySide import QtGui, QtCore

from config import Config
from visibility_button import VisibilityButton


class SeriesName(QtGui.QToolButton):
    sig_update = QtCore.Signal()

    def __init__(self, parent, series):
        """Initialize the button."""
        QtGui.QToolButton.__init__(self)
        self.setParent(parent)

        self.name = ""
        self.series = series
        self.config = Config()

        self.setAutoRaise(True)

        self.x_list = []
        self.y_list = []
        self.y_axis_list = []
        self.prepare_selection()

        self.menu = None
        self.edit_style = None
        self.edit_x = None
        self.edit_y = None
        self.edit_yaxis = None
        self.button_ok = None
        self.create_context_menu()

    def prepare_selection(self):
        """Prepare lists of X and Y axis and series."""
        try:
            self.x_list = self.config.list_x_axis()
            self.y_list = self.config.get_columns()
            self.y_axis_list = self.config.list_y_axis()
        finally:
            self.setText(self.series.name)

    @staticmethod
    def create_menu_edit(parent, label, value, callback):
        """Create a context menu entry for a textbox."""
        container = QtGui.QWidget(parent)
        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(2, 2, 2, 2)
        
        e_label = QtGui.QLabel(container)
        e_label.setText(label)
        
        e_edit = QtGui.QLineEdit(container)
        e_edit.setText(value)
        e_edit.textChanged.connect(callback)
        
        layout.addWidget(e_label)
        layout.addWidget(e_edit)
        
        container.setLayout(layout)
        
        widget_action = QtGui.QWidgetAction(parent)
        widget_action.setDefaultWidget(container)
        return widget_action, e_edit

    @staticmethod
    def create_menu_combobox(parent, label, entries, selection, callback, editable=False, edit_callback=None):
        """Create a context menu entry for selecting something from some list."""
        container = QtGui.QWidget(parent)
        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(2, 2, 2, 2)
        
        x_label = QtGui.QLabel(container)
        x_label.setText(label)
        
        x_edit = QtGui.QComboBox(container)
        for i in range(0, len(entries)):
            x_edit.addItem(entries[i], i)
        
        x_edit.setCurrentIndex(selection)
        x_edit.currentIndexChanged.connect(callback)

        x_edit.setEditable(editable)
        if editable:
            x_edit.editTextChanged.connect(edit_callback)
        
        layout.addWidget(x_label)
        layout.addWidget(x_edit)
        
        container.setLayout(layout)
        
        widget_action = QtGui.QWidgetAction(parent)
        widget_action.setDefaultWidget(container)
        return widget_action, x_edit

    def create_menu_okbutton(self, parent):
        """Create a context menu entry for an OK button."""
        ok_button = QtGui.QPushButton("Ok")
        ok_button.clicked.connect(self.do_update)
        
        widget_action = QtGui.QWidgetAction(parent)
        widget_action.setDefaultWidget(ok_button)
        return widget_action, ok_button

    def create_context_menu(self):
        """Custom context menu on right-click."""
        # Thanks to http://www.ffuts.org/blog/right-click-context-menus-with-qt/
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.show_context_menu)

        # Create a menu
        self.menu = QtGui.QMenu(self)

        # Note: The list of Y-Axes might change at runtime.
        y_axis_list = self.config.list_y_axis()
        # Find the index of the configured Y-Axis.
        y_axis = 0
        for i in range(0, len(y_axis_list)):
            if y_axis_list[i] == self.series.y_axis_name:
                y_axis = i

        style_edit, self.edit_style = self.create_menu_edit(
            self, "Style:",
            self.series.style,
            self.style_changed
        )
        x_edit, self.edit_x = self.create_menu_combobox(
            self, "X:",
            self.x_list,
            self.series.x,
            self.x_selected
        )
        y_edit, self.edit_y = self.create_menu_combobox(
            self, "Y:",
            self.y_list,
            self.series.y,
            self.y_selected
        )
        yaxis_edit, self.edit_yaxis = self.create_menu_combobox(
            self, "Y-Axis:",
            self.y_axis_list,
            y_axis,
            self.yaxis_selected,
            True, self.yaxis_edited
        )
        self.menu.addAction(style_edit)
        self.menu.addAction(x_edit)
        self.menu.addAction(y_edit)
        self.menu.addAction(yaxis_edit)

        ok_button, self.button_ok = self.create_menu_okbutton(self)
        self.menu.addAction(ok_button)

    def show_context_menu(self, pos):
        """Show context menu that allows for editing the refresh period."""
        # Make sure that the menu is created at the click
        g_pos = self.mapToGlobal(pos)
        # Show it at the right position
        self.menu.exec_(g_pos)

    def mousePressEvent(self, event):
        QtGui.QToolButton.mousePressEvent(self, event)
        if event.button() == QtCore.Qt.LeftButton:
            QtGui.QToolButton.mouseReleaseEvent(self, event)
            self.show_context_menu(event.pos())
    
    def x_selected(self, new_x):
        """Series X selection changed."""
        self.series.set_x_id(new_x)

    def y_selected(self, new_y):
        """Series Y selection changed."""
        self.series.set_y_id(new_y)
        # Update the Y-Axis selection as well.
        print self.series.y_axis_name
        index = self.edit_yaxis.findText(self.series.y_axis_name)
        self.edit_yaxis.setCurrentIndex(index)

    def yaxis_selected(self, new_yaxis):
        """Y-Axis selection changed."""
        new_yaxis_name = self.y_axis_list[new_yaxis]
        self.series.set_y_axis_name(new_yaxis_name)

    def yaxis_edited(self, new_yaxis_name):
        """The Y-Axis name was manually edited? Perhaps should create a new Y-Axis?"""
        if new_yaxis_name in self.y_axis_list:
            self.series.set_y_axis_name(new_yaxis_name)
        else:
            self.series.y_axis = len(self.y_axis_list)
            self.series.y_axis_name = new_yaxis_name

    def style_changed(self, new_style):
        """Update line style."""
        self.series.set_style(new_style)

    def do_update(self):
        """Close the menu and propagate update signals."""
        self.menu.hide()
        self.config.update_yaxis()
        self.update_name()
        self.sig_update.emit()

    def update_name(self):
        """Update the series name and tooltip, based on the selections."""
        self.name = self.series.y_name
        self.setText(self.series.name)
        self.setToolTip("({}, {}, {})".format(self.series.x_name, self.series.y_name, self.series.y_axis_name))

    def get_name(self):
        """Get series name."""
        return self.series.name


class SeriesWidget(QtGui.QFrame):
    sig_series_update = QtCore.Signal(object)
    sig_series_remove = QtCore.Signal(object)
    sig_series_visibility = QtCore.Signal(object, bool)

    matplotlib_css_color_map = {
        "k": "black",
        "b": "blue",
        "g": "green",
        "c": "cyan",
        "m": "magenta",
        "y": "yellow"
    }

    def __init__(self, parent, series):
        """Initialize the series widget."""
        QtGui.QFrame.__init__(self)
        self.setParent(parent)

        self.setObjectName("Series")
        self.config = Config()
        self.series = series

        self.layout = QtGui.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        self.vis_button = VisibilityButton(self)
        self.vis_button.sig_state_changed.connect(self.change_series_visibility)
        self.layout.addWidget(self.vis_button)

        self.name_widget = SeriesName(self, series)
        self.name_widget.sig_update.connect(self.series_updated)
        self.layout.addWidget(self.name_widget)

        self.close_button = QtGui.QToolButton()
        self.close_button.setIcon(QtGui.QIcon("pyvisualizer/icons/cross.png"))
        self.close_button.setAutoRaise(True)
        self.close_button.clicked.connect(self.do_remove)
        self.layout.addWidget(self.close_button)

        self.setLayout(self.layout)
        self.update_style()

    def do_remove(self):
        """Clean up and ask for the config panel to remove us."""
        self.hide()

        self.layout.removeWidget(self.name_widget)
        del self.name_widget

        self.layout.removeWidget(self.close_button)
        del self.close_button

        self.config.remove_series(self.series)
        self.sig_series_remove.emit(self)

    def update_style(self):
        """Update border color around the widget."""
        if self.series.style[0] in self.matplotlib_css_color_map:
            color = self.matplotlib_css_color_map[self.series.style[0]]
        else:
            color = "black"

        self.setStyleSheet(
            "QFrame#Series {{"
            "    border-style: solid;"
            "    border-width: 1px;"
            "    border-color: {};"
            "}}"
            .format(color)
        )

    def series_updated(self):
        """Notify others that the series settings were updated."""
        self.update_style()
        self.sig_series_update.emit(self.series)

    def change_series_visibility(self, new_state):
        """Show or hide a series."""
        is_visible = (new_state == VisibilityButton.STATE_SHOWN)
        self.series.visible = is_visible
        self.sig_series_visibility.emit(self.series, is_visible)
