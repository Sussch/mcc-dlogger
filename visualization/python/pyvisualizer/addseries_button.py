#!/usr/bin/python
#
# Python Visualizer button for adding series.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from PySide import QtGui


class AddSeriesButton(QtGui.QToolButton):

    def __init__(self, parent):
        """Initialize the add-series button."""
        QtGui.QToolButton.__init__(self)
        self.setParent(parent)

        self.setIcon(QtGui.QIcon("pyvisualizer/icons/add.png"))
        self.setAutoRaise(True)
        self.setToolTip("Add a series")
