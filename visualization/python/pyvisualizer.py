#!/usr/bin/python
#
# Python Visualizer for the MCC Data Logger.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging
import logging.handlers
import argparse
import os
import sys
from PySide import QtGui, QtCore

from pyvisualizer.window import PVMainWindow


# TODO:: Figure out how to share common code with DLogger, without introducing dependencies.

class LinuxLogColorFormatter(logging.Formatter):
    """Python logging color formatter.
    http://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
    """
    FORMAT = ("[%(levelname)-18s][%(name)-20s]  "
              "%(message)s "
              "($BOLD%(filename)s$RESET:%(lineno)d)")
    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

    COLORS = {
        'WARNING': YELLOW,
        'INFO': WHITE,
        'DEBUG': BLUE,
        'CRITICAL': YELLOW,
        'ERROR': RED,
        'RED': RED,
        'GREEN': GREEN,
        'YELLOW': YELLOW,
        'BLUE': BLUE,
        'MAGENTA': MAGENTA,
        'CYAN': CYAN,
        'WHITE': WHITE,
    }

    RESET_SEQ = "\033[0m"
    COLOR_SEQ = "\033[1;%dm"
    BOLD_SEQ = "\033[1m"

    def __init__(self, use_color=True):
        """Initialize the formatter."""
        self.use_color = use_color
        if use_color:
            msg = self.FORMAT.replace("$RESET", self.RESET_SEQ).replace("$BOLD", self.BOLD_SEQ)
        else:
            msg = self.FORMAT.replace("$RESET", "").replace("$BOLD", "")

        logging.Formatter.__init__(self, msg)

    def format(self, record):
        """Format a log record."""
        levelname = record.levelname
        if self.use_color and levelname in self.COLORS:
            fg_color = 30 + self.COLORS[levelname]
            levelname_color = self.COLOR_SEQ % fg_color + levelname + self.RESET_SEQ
            record.levelname = levelname_color
        return logging.Formatter.format(self, record)


class LogInfoFilter(logging.Filter):
    """A logging filter that discards warnings and errors."""
    def filter(self, record):
        """False on warning or error records."""
        return record.levelno in (logging.DEBUG, logging.INFO)


def init_logging(opts, logfile):
    """Initialize logging, based on verbosity level."""
    # Configure logging
    if opts.verbose == 0:
        log_level = logging.WARNING
    elif opts.verbose == 1:
        log_level = logging.INFO
    elif opts.verbose == 2:
        log_level = logging.DEBUG
    else:
        log_level = logging.NOTSET

    log = logging.getLogger("PV")

    # Create log formatters.
    log_formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(name)s: %(message)s')
    if os.name != 'nt':
        stdout_formatter = LinuxLogColorFormatter()
    else:
        # For colored output in Windows, refer to sorin's answer in
        # http://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
        stdout_formatter = logging.Formatter('%(levelname)s: %(name)s: %(message)s')

    # Info, debug messages to stdout (if we have enough verbosity).
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(log_level)
    stdout_handler.setFormatter(stdout_formatter)
    stdout_handler.addFilter(LogInfoFilter())

    # Warnings and errors to stderr.
    stderr_handler = logging.StreamHandler(sys.stderr)
    stderr_handler.setLevel(logging.WARNING)
    stderr_handler.setFormatter(stdout_formatter)

    # Rotating log up to 1 MB.
    log_handler = logging.handlers.RotatingFileHandler(logfile, maxBytes=1024*1024, backupCount=3)
    log_handler.setLevel(logging.DEBUG)
    log_handler.setFormatter(log_formatter)

    # Add the stream handlers.
    log.addHandler(log_handler)
    log.addHandler(stderr_handler)
    log.addHandler(stdout_handler)

    log.info('MCC DLogger PyVisualizer started..')

    return log


def main():
    """Application main loop, which stops with Ctrl+Q."""
    p = argparse.ArgumentParser()
    p.add_argument("-i", "--infile", dest="infile", default="")
    p.add_argument("-c", "--cfg", dest="cfgfile", default="")
    p.add_argument("-p", "--period", dest="period", default=0.0)
    p.add_argument("-l", "--log", dest="logfile", default="pyvisualizer.log")
    p.add_argument("-v", "--verbose", dest="verbose", action="count", 
                   help="Increase verbosity (specify multiple times for more)")

    arguments = p.parse_args()

    log = None
    mainwin = None
    try:
        log = init_logging(arguments, arguments.logfile)

        app = QtGui.QApplication(sys.argv)
        app.setOrganizationDomain("www.to.ee")
        app.setOrganizationName("Tartu Observatory")
        app.setApplicationName("MCC DLogger Python Visualizer")
        app.setApplicationVersion("0.01")

        mainwin = PVMainWindow()
        mainwin.show()

        app.connect(app, QtCore.SIGNAL("lastWindowClosed()"),
                    app, QtCore.SLOT("quit()"))
        app.exec_()

    except Exception as e:
        if log is not None:
            log.exception("Unhandled exception")
        else:
            print "Failed to initialize error logging: " + str(e)
        # Produce a pop-up to show the error as well.
        if not mainwin:
            mainwin = QtGui.QMainWindow()
        error_dialog = QtGui.QErrorMessage(mainwin)
        error_dialog.setWindowTitle("Error")
        error_dialog.showMessage(str(e))


if __name__ == '__main__':
    main()
